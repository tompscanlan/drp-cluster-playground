#!/usr/bin/env just --justfile

clean:
    rm -f .deploy.yaml

bundle:
    meta=content/meta.yaml; \
    version=$(yq  ".Version" < $meta); \
    new_version=$(echo $version | awk -F"-" '{printf "%s-%d", $1, $2+1}'); \
    yq eval -i ".Version = \"$new_version\"" $meta; \
    cd content && drpcli contents bundle ../.deploy.yaml

deploy: bundle
    cd content && drpcli contents upload ../.deploy.yaml

undeploy:
    drpcli contents destroy ridge-road-lab

lint: bundle
    yamllint content/meta.yaml
